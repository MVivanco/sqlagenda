package com.example.sqlagenda;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sqlagenda.R;

import database.AgendaContactos;
import database.AgendaDBHelper;
import database.Contacto;

public class MainActivity extends AppCompatActivity {
    private AgendaContactos db;
    private EditText edtNombre;
    private EditText edtTelefono;
    private EditText edtTelefono2;
    private EditText edtDireccion;
    private EditText edtNotas;
    private CheckBox cbxFavorito;
    long id = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        db = new AgendaContactos(MainActivity.this);
        edtNombre = (EditText) findViewById(R.id.edtNombre);
        edtTelefono = (EditText) findViewById(R.id.edtTel1);
        edtTelefono2 = (EditText) findViewById(R.id.edtTel2);
        edtDireccion = (EditText) findViewById(R.id.edtDomicilio);
        edtNotas = (EditText) findViewById(R.id.edtNota);
        cbxFavorito = (CheckBox) findViewById(R.id.chkFavorito);
        Button btnGuardar = (Button) findViewById(R.id.btnGuardar);
        Button btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        Button btnListar = (Button) findViewById(R.id.btnListar);


        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiar();
            }
        });

        btnListar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ListActivity.class);
                startActivityForResult(intent, 0);
            }
        });

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(edtNombre.getText().toString().matches("") || edtTelefono.getText().toString().matches("") ||
                        edtDireccion.getText().toString().matches("")) {
                    Toast.makeText(MainActivity.this, "Favor de llenar todos los campos", Toast.LENGTH_SHORT).show();
                } else {
                    Contacto nContacto = new Contacto();
                    nContacto.setNombre(edtNombre.getText().toString());
                    nContacto.setDomicilio(edtDireccion.getText().toString());
                    nContacto.setNotas(edtNotas.getText().toString());
                    nContacto.setTelefono1(edtTelefono.getText().toString());
                    nContacto.setTelefono2(edtTelefono2.getText().toString());
                    nContacto.setFavorito(cbxFavorito.isChecked() ? 1 : 0);

                    db.openDataBase();

                    if (id >= 0) {
                        db.UpdateContacto(nContacto, id);
                        Toast.makeText(MainActivity.this, "Se actualizó el registro: " + id, Toast.LENGTH_SHORT).show();
                        id = -1;
                    }else{
                        long ret = db.insertarContacto(nContacto);
                        Toast.makeText(MainActivity.this, "Se agregó el nuevo contacto", Toast.LENGTH_SHORT).show();
                    }

                    db.cerrarDataBase();
                    limpiar();
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if(intent != null) {
            Bundle oBundle = intent.getExtras();

            if(oBundle.getSerializable("contacto") instanceof Contacto) {
                Contacto contacto = (Contacto) oBundle.getSerializable("contacto");
                id=contacto.getID();
                edtNombre.setText(contacto.getNombre());
                edtDireccion.setText(contacto.getDomicilio());
                edtNotas.setText(contacto.getNotas());
                edtTelefono.setText(contacto.getTelefono1());
                edtTelefono2.setText(contacto.getTelefono2());
                cbxFavorito.setChecked(contacto.getFavorito() > 0 ? true : false);
            }
            boolean nuevo = oBundle.getBoolean("nuevo");
            if(nuevo){
                limpiar();
            }
        }
    }

    public void limpiar() {
        edtNombre.setText("");
        edtDireccion.setText("");
        edtTelefono.setText("");
        edtTelefono2.setText("");
        edtNotas.setText("");
        cbxFavorito.setChecked(false);
        id = -1;
    }
}
