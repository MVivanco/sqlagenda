package com.example.sqlagenda;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import database.AgendaContactos;
import database.Contacto;

public class ListActivity extends android.app.ListActivity {
    private AgendaContactos agendaContacto;
    private Button btnNuevo;
    MyArrayAdapter adapter;
    ArrayList<Contacto> listaContacto;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        btnNuevo = (Button) findViewById(R.id.btnNuevo);
        agendaContacto = new AgendaContactos(this);

        llenarLista();
        adapter = new MyArrayAdapter(this, R.layout.layout_contacto, listaContacto);
        setListAdapter(adapter);

        btnNuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putSerializable("nuevo", true);
                Intent i = new Intent();
                i.putExtras(bundle);
                setResult(Activity.RESULT_OK, i);
                finish();
            }
        });
    }

    private void llenarLista(){
        agendaContacto.openDataBase();
        listaContacto = agendaContacto.allContactos();
        agendaContacto.cerrarDataBase();
    }

    class MyArrayAdapter extends ArrayAdapter<Contacto> {
        private Context context;
        private int textViewResourceId;
        private ArrayList<Contacto> contactos;
        private LayoutInflater inflater;

        public MyArrayAdapter(Context context, int textViewResourceId, ArrayList<Contacto> contactos) {
            super(context, textViewResourceId, contactos);
            this.context = context;
            this.textViewResourceId = textViewResourceId;
            this.contactos = contactos;
            this.inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            View view = this.inflater.inflate(this.textViewResourceId, null);
            TextView lblNombre = (TextView) view.findViewById(R.id.lblNombre);
            TextView lblTelefono = (TextView) view.findViewById(R.id.lblTelefonoContacto);

            Button btnModificar = (Button) view.findViewById(R.id.btnModificar);
            Button btnBorrar = (Button) view.findViewById(R.id.btnBorrar);

            if(contactos.get(position).getFavorito()>0) {
                lblNombre.setTextColor((Color.BLUE));
                lblTelefono.setTextColor(Color.BLUE);

            } else {
                lblNombre.setText(contactos.get(position).getNombre());
                lblTelefono.setTextColor(Color.BLACK);
            }
            lblNombre.setText(contactos.get(position).getNombre());
            lblTelefono.setText(contactos.get(position).getTelefono1());

            btnBorrar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    agendaContacto.openDataBase();
                    agendaContacto.deleteContacto(contactos.get(position).getID());
                    agendaContacto.cerrarDataBase();
                    contactos.remove(position);
                    notifyDataSetChanged();
                }
            });

            btnModificar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("contacto",contactos.get(position));
                    Intent i = new Intent();
                    i.putExtras(bundle);
                    setResult(Activity.RESULT_OK, i);
                    finish();
                }
            });

            return  view;
        }
    }
}
